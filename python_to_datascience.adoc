= Python to Datascience
Karthikeyan Arcot Kuppusamy <mindaslab@protonmail.com>
A Practical Guided Journey.
:source-highlighter: highlightjs
:toc: left

include::preface.adoc[]
include::i.adoc[]
include::what_is_datascience.adoc[]
include::system_requirements.adoc[]
include::join_the_community.adoc[]
include::python.adoc[]
include::references.adoc[]
include::donate.adoc[]

