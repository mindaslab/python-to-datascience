== Python

http://python.org/[Python programming language] has become the language of choice for Data Scientist around the world. Its simple to learn and is easy to use. Lot of datascience libraries and machine learning algorithms have a Python interface. We will be seeing Python in this section.

=== Installation

there are many ways to get Python running in GNU/Linux, but we recommend Anaconda, it installs Python and many other data science tools. Anaconda can be obtained here https://www.anaconda.com/download/#linux

At the time of writing this chapter, you can install it by typing these in terminal

[source,bash]
----
$ wget https://repo.anaconda.com/archive/Anaconda3-5.2.0-Linux-x86_64.sh
$ chmod a+x ./Anaconda3-5.2.0-Linux-x86_64.sh
$./Anaconda3-5.2.0-Linux-x86_64.sh
----

The installer will guide you through the installation procedure. Once done, close the terminal and reopen it and do the following

[source,bash]
----
$ python
Python 3.6.4 |Anaconda, Inc.| (default, Jan 16 2018, 18:10:19) 
[GCC 7.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
----

For typing python, if you get output as shown above, then you have done it right. You can quit python by typing `quit()` or pressing `ctrl+D`.

include::math_in_python.adoc[]
include::variables.adoc[]
include::using_text_editor.adoc[]
include::datastructures.adoc[]

